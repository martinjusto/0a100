#Librerías:
import sys
import time
from functions.create import create
from functions.read import read
from functions.general import *

# Hilo principal
def index():
    while True:
        if check()==True:  # Nos aseguramos de que el fichero existe.
            o=int(input("\nPor favor, indica el número de la acción que deseas relizar:\n"
                    "  1. Ver lista de empleados.\n"
                    "  2. Dar de alta un empleado\n"
                    "  3. Salir\n"))
            if o == 1:
                read()
            if o == 2:
                create()
            if o == 3:
                print("Gracias por usar nuestro software.")
                sys.exit(0)



try:
    index()
except ValueError:
    index()
except:
    print("Error desconocido. Se va a relanzará el programa en 5 segundos.")
    time.sleep(5)
    index()
