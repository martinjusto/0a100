#Librerías
import json
from datetime import date

#Clases
class employee:
    def __init__(self):
        self.name=str(input("Nombre:"))
        self.sname1=str(input("\nPrimer apellido:"))
        self.sname2=str(input("\nSegundo apellido:"))
        self.bdate=str(input("\nFecha de nacimiento:"))
        self.idate=str(date.today()) #Siempre se pondrá por defecto la fecha actual como fecha de ingreso.
        self.salary=str(input("\nSalario (Sólo número entero, ej 10000):"))
        self.nation=str(input("\nNacionalidad:"))

#Funciones
def sjson (reg):
    with open("data\employees.json") as f:
        data = json.load(f)

        data['employee'].append({
            'name': reg.name,
            'sname1': reg.sname1,
            'sname2': reg.sname2,
            'bdate': reg.bdate,
            'idate': reg.idate,
            'salary': reg.salary,
            'nation': reg.nation})

        with open("data\employees.json", "w") as f:  # Guardamos los datos.
            f.write(json.dumps(data, indent=4, separators=(",", ": ")))
        return True

def create():
    print("Introduce los datos del empleado que deseas añadir:\n")
    reg=employee()      #Creamos un registro de clase employee con los datos obtenidos.

    if sjson(reg):
        print("\n\n"+reg.name+" "+ reg.sname1 + " se ha registrado correctamente.\n\n") #Confirmamos el registro al usuario.




