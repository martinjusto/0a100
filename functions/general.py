#Librerías:
import os
import json

#Funciones:
def check(): #Comprueba si existe el fichero de registro de empleados.
    if os.path.isfile("data\employees.json"):
        return True
    else:                                       #Si el fichero no existe.
        var=str(input("No se ha encontrado un registro de empleados.\n"
                    "¿Deseas crear uno ahora? *s/n*. *Exit* volver al inicio.\n"))
        var=var.lower()
        if var==("exit"):
            print("")
        elif var==("s"):
            data = {}
            data['employee'] = []
            with open('data\employees.json', 'w') as file:
                json.dump(data, file, indent=4)
            print ("Se ha creado el fichero. ¿Qué quieres hacer?\n")
        elif var==("n"):
            print("\nRegistro no creado, volviendo al menú principal.\n")
        else:
            print("\n\nNo se reconoce tu respuesta\n")
            check()
        return True
