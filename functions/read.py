#Librerías
import json
from datetime import date, datetime

#Funciones
def idays(d):
    return abs(datetime.today() - d).days

def employeeData(data):
    d=datetime.strptime(data['idate'], '%Y-%m-%d')
    print("\n\n---------------------------"
          "\nNombre: ",data['name'],
          "\nPrimer apellido: ",data['sname1'],
          "\nSegundo apellido: ",data['sname2'],
          "\nNacionalidad: ", data['nation'],
          "\nFecha de nacimiento: ",data['bdate'],
          "\nFecha de incorporación: ",data['idate'], "(",idays(d)," días)",
          "\nSalario: ",data['salary'],
          "\n---------------------------\n")


def read ():
    with open("data\employees.json") as f:
        data = json.load(f)
        print("Id  - Usuario\n-------------")
        for i in range(0,len(data['employee'])):
            print(i+1,"  - "+data['employee'][i]['name']+" "+data['employee'][i]['sname1'])

    id=int(input("Si desea ver más información del empleado indique su ID (1-x).\nSi quiere salir escriba '0': "))
    if id==0:
        pass
    elif (id>0)and(id<len(data['employee'])):
        user=data['employee'][id-1]     #Obtenemos la ficha del usuario que nos piden.
        employeeData(user)
    else:
        print("La id que has indicado no se encuentra registrada")
